#############################
# Kubo-CI Develop - NodeJS
#############################

include:
  - remote: "https://gitlab.com/kubozoa/public/kubo-operate/kubo-ci/raw/main/develop/common/artifacts.yml"
  - remote: "https://gitlab.com/kubozoa/public/kubo-operate/kubo-ci/raw/main/develop/common/sca.yml"

# Runner tag
default:
  tags:
    - $KUBO_CI_RUNNER_TAG

variables:
  ##### Module configuration #####
  DEV_NJS_IMAGE: $KUBO_CI_DEVELOP_NJS20_IMAGE
  DEV_NJS_ROOT: "node"

  ##### Module variables (default values) #####
  DEV_NJS_LINT_ALLOW_FAILURE: "true"

  DEV_NJS_BUILD_CMD: "npm install"
  
  DEV_NJS_TEST_CMD: ""
  DEV_NJS_TEST_ALLOW_FAILURE: "false"
  
  DEV_NJS_BUILD_PACKAGE: "true"
  # Only for snapshots. If tagged, the CI_COMMIT_TAG is used as the package version.
  DEV_NJS_SNAPSHOT_PACKAGE_VERSION: "0.0.1-b1"

  ##### SCA Customizations #####
  DEV_SCA_PROJECT_LANG: "nodejs"
  DEV_SCA_ROOT: ${DEV_NJS_ROOT}

# Stages  
stages:
  - lint
  - build
  - test
  - sca
  - package
  - deploy

###################################################################################################################
### Auth ###

.npm_auth: &npm_auth
  # Kubozoa
  - echo -e "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/packages/npm/" > .npmrc
  - echo -e "//gitlab.com/api/v4/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
  # Run all acceptance test cases as defined
  - export NPM_TOKEN=${CI_JOB_TOKEN}

###################################################################################################################

###################################################################################################################
### Lint ###
###################################################################################################################

eslint:
  stage: lint
  image: 
    name: $DEV_NJS_IMAGE
    entrypoint: [""]
  script:
    - *npm_auth
    - npm install eslint
    - if [ -f "$DEV_NJS_ROOT/eslint.config.js" ]; then
        npx eslint "$DEV_NJS_ROOT";
      else
        npx eslint --no-config-lookup "$DEV_NJS_ROOT";
      fi
  rules:
    - if: '$DEV_NJS_LINT_ALLOW_FAILURE == "true"'
      allow_failure: true
    - if: '$DEV_NJS_LINT_ALLOW_FAILURE == "false"'
      allow_failure: false
    - when: always

###################################################################################################################
### Build ###
###################################################################################################################

install-deps:
  stage: build
  image: 
    name: $DEV_NJS_IMAGE
    entrypoint: [""]
  script:
    - *npm_auth
    - cd ${DEV_NJS_ROOT}
    - eval "${DEV_NJS_BUILD_CMD}"

###################################################################################################################
### Test ###
###################################################################################################################

unit-tests:
  stage: test
  image: 
    name: $DEV_NJS_IMAGE
    entrypoint: [""]
  before_script:
    - mkdir -p $CI_PROJECT_DIR/coverage/
  script:
    - *npm_auth
    - cd $DEV_NJS_ROOT
    - eval "${DEV_NJS_TEST_CMD}"
  rules:
    - if: '$DEV_NJS_TEST_CMD == ""'
      when: never  
    - if: '$DEV_NJS_TEST_ALLOW_FAILURE == "true"'
      allow_failure: true
    - if: '$DEV_NJS_TEST_ALLOW_FAILURE == "false"'
      allow_failure: false
  artifacts:
    paths:
      - $CI_PROJECT_DIR/coverage/

###################################################################################################################
### SCA ###
###################################################################################################################

# common/sca.yml


###################################################################################################################
### Package ###
###################################################################################################################

.build-package:
  stage: package
  image: 
    name: $DEV_NJS_IMAGE
    entrypoint: [""]
  before_script:
    - apt-get update && apt-get install -y jq  
  script:
    - *npm_auth
    - cd $DEV_NJS_ROOT
    - |
      if [ -n "${CI_COMMIT_TAG}" ]; then
        PACKAGE_VERSION="${CI_COMMIT_TAG}"
      else
        PACKAGE_VERSION="${DEV_NJS_SNAPSHOT_PACKAGE_VERSION}"
      fi
    - jq ".version=\"$PACKAGE_VERSION\"" package.json > temp.json && mv temp.json package.json
    - echo "Version set to ${PACKAGE_VERSION}"
    - eval "${DEV_NJS_BUILD_CMD}"
    - |
      # Capture the generated .tgz filename
      PACKAGE_NAME="$( npm pack )"
      echo "Package created: $PACKAGE_NAME"
      mkdir -p dist && mv "$PACKAGE_NAME" dist/
  artifacts:
    paths:
      - dist/


build-feature-package:
  stage: package
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_TAG == null && $DEV_NJS_BUILD_PACKAGE == "true"'
      when: always
  extends: .build-package

build-release-package:
  stage: package
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_TAG == null'
      when: never
    - if: '$DEV_NJS_BUILD_PACKAGE == "true"'
      when: always
  extends: .build-package

###################################################################################################################
### Deploy ###
###################################################################################################################

.deploy-package:
  stage: deploy
  image: 
    name: $DEV_NJS_IMAGE
    entrypoint: [""]
  script:
    - *npm_auth
    - cd $DEV_NJS_ROOT
    - |
      if [ -n "${CI_COMMIT_TAG}" ]; then
        PACKAGE_VERSION="${CI_COMMIT_TAG}"
      else
        PACKAGE_VERSION="${DEV_NJS_SNAPSHOT_PACKAGE_VERSION}"
      fi
    - jq ".version=\"$PACKAGE_VERSION\"" package.json > temp.json && mv temp.json package.json
    - echo "Version set to ${PACKAGE_VERSION}"
    - eval "${DEV_NJS_BUILD_CMD}"
    # TODO: Support configurable target repo here.
    - echo -e "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/" > .npmrc
    - echo -e "${CI_API_V4_URL#https?}/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
    - npm publish
  extends: .clean-existing-artifact
  variables:
    SNAPSHOT_PACKAGE_VERSION: ${DEV_NJS_SNAPSHOT_PACKAGE_VERSION}
    PACKAGE_TYPE: npm

deploy-feature-package:
  rules:
    - if: '$CI_COMMIT_TAG == null && $DEV_NJS_BUILD_PACKAGE == "true"'
  needs:
    - job: build-feature-package
      artifacts: true
  extends: .deploy-package

deploy-dev-package:
  environment: dev
  rules:
    - if: '$CI_COMMIT_TAG =~ /^(\d+\.?){3}[ab]\d+$/ && $DEV_NJS_BUILD_PACKAGE == "true"'
  needs:
    - job: build-release-package
      artifacts: true
  extends: .deploy-package

deploy-rc-package:
  environment: stg
  rules:
    - if: '$CI_COMMIT_TAG =~ /^(\d+\.?){3}rc\d+$/ && $DEV_NJS_BUILD_PACKAGE == "true"'
  needs:
    - job: build-release-package
      artifacts: true
  extends: .deploy-package

deploy-prod-package:
  environment: prod
  rules:
    - if: '$CI_COMMIT_TAG =~ /^(\d+\.?){3}$/ && $DEV_NJS_BUILD_PACKAGE == "true"'
  extends: .deploy-package
  needs:
    - job: build-release-package
      artifacts: true
