#!/bin/bash

# Script Settings
SCRIPT=$( realpath "$0" )
SCRIPT_NAME=$( basename "${SCRIPT}" )
LOG_FILE="/var/log/${SCRIPT_NAME%.*}.log"

# Cluster settings
CLUSTER_DATA_FOLDER="/mnt/data"
CLUSTER_HOME_FOLDER="/opt/kubozoa"
CLUSTER_NFS_VOLUME="/volumes"
CLUSTER_CONFIG_FOLDER="${CLUSTER_DATA_FOLDER}/config"
CLUSTER_VOLUME_FOLDER="${CLUSTER_DATA_FOLDER}${CLUSTER_NFS_VOLUME}"
CLUSTER_LOCK_FILE="${CLUSTER_CONFIG_FOLDER}/.lock"
CLUSTER_NODE_FILE="/etc/cluster_node"
if [ -d ${CLUSTER_DATA_FOLDER} ]; then
  echo "Cluster data folder found: ${CLUSTER_DATA_FOLDER}" >> ${LOG_FILE}
else
  mkdir -p ${CLUSTER_DATA_FOLDER}
fi
if [ -d ${CLUSTER_HOME_FOLDER} ]; then
  echo "Cluster home folder found: ${CLUSTER_HOME_FOLDER}" >> ${LOG_FILE}
else
  mkdir -p ${CLUSTER_HOME_FOLDER}
fi

# Cluster environment
source ${CLUSTER_HOME_FOLDER}/cluster.env

# Kubo-CI URL
KUBO_CI_URL="https://gitlab.com/kubozoa/public/kubo-operate/kubo-ci/raw/main/operate/terraform"

#
# Kernel Parameters
#

echo -n "Updating kernel parameters... " >> ${LOG_FILE}
echo "vm.max_map_count=262144" > /etc/sysctl.d/cluster.conf
echo "vm.swappiness=20" >> /etc/sysctl.d/cluster.conf
sysctl --system
echo "OK" >> ${LOG_FILE}

#
# Disable Firewall Service
#

echo -n "Disabling firewall service... " >> ${LOG_FILE}
if [ -n "$( which ufw )" ]; then
  systemctl stop ufw
  systemctl disable ufw
elif [ -n "$( which firewalld )" ]; then
  systemctl stop firewalld
  systemctl disable firewalld
fi
echo "OK" >> ${LOG_FILE}

#
# Swap Space
#

echo -n "Creating and mounting swap... " >> ${LOG_FILE}
fallocate -l 2g /mnt/2GiB.swap
chmod 600 /mnt/2GiB.swap
mkswap /mnt/2GiB.swap
swapon /mnt/2GiB.swap
echo '/mnt/2GiB.swap swap swap defaults 0 0' | sudo tee -a /etc/fstab
echo "OK" >> ${LOG_FILE}

#
# Install packages
#

echo "Installing packages:" >> ${LOG_FILE}
curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
while [ -n "$( ps -ef | grep ' apt' | grep -v 'grep' )" ]; do
  echo "Waiting for lock..."
  sleep 5
done
apt-get -o DPkg::Lock::Timeout=300 -y update >> ${LOG_FILE} 2>&1
# snapd and NFS
echo "-> snapd and NFS..."
apt-get -o DPkg::Lock::Timeout=300 -y install net-tools nfs-common snapd unzip >> ${LOG_FILE} 2>&1
# microk8s
echo "-> microk8s..."
snap install microk8s --classic >> ${LOG_FILE} 2>&1
# AWS CLI (TODO: add gcloud)
echo "-> AWS CLI..."
curl -Ls https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o /tmp/awscliv2.zip >> ${LOG_FILE} 2>&1
unzip -q /tmp/awscliv2.zip -d /tmp >> ${LOG_FILE} 2>&1
/tmp/aws/install >> ${LOG_FILE} 2>&1

#
# Mount NFS Volume (if defined)
#

# AWS (TODO: add GCP)
if [ -n "${EFS_VOLUME_ID}" ]; then
  AZ=$( curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone )
  REGION="${AZ%?}"
  CLUSTER_NFS_SERVER="${AZ}.${EFS_VOLUME_ID}.efs.${REGION}.amazonaws.com"
  echo -n "Mounting cluster folder (ID: ${EFS_VOLUME_ID})... " >> ${LOG_FILE}
  echo "${CLUSTER_NFS_SERVER}:/ ${CLUSTER_DATA_FOLDER} nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0" | tee -a /etc/fstab
  mount -a
  if [ "$( mount | grep ${CLUSTER_DATA_FOLDER} )" = "" ]; then
    echo "FAILED" >> ${LOG_FILE}
    exit
  fi
  echo "OK" >> ${LOG_FILE}
fi
if [ -d ${CLUSTER_CONFIG_FOLDER} ]; then
  echo "Cluster config folder found: ${CLUSTER_CONFIG_FOLDER}" >> ${LOG_FILE}
else
  mkdir -p ${CLUSTER_CONFIG_FOLDER}
fi
if [ -d ${CLUSTER_VOLUME_FOLDER} ]; then
  echo "Cluster volume folder found: ${CLUSTER_VOLUME_FOLDER}" >> ${LOG_FILE}
  echo "Existing volumes:" >> ${LOG_FILE}
  ls -l ${CLUSTER_VOLUME_FOLDER} >> ${LOG_FILE}
else
  mkdir -p ${CLUSTER_VOLUME_FOLDER}
fi

#
# Download Files
#

function download_file() {

  local SRC_FILENAME="$1"
  local DST_FOLDER="$2"
  local DST_FILENAME="$3"

  if [ ! -d "${DST_FOLDER}" ]; then
    mkdir -p "${DST_FOLDER}"
    chmod 777 "${DST_FOLDER}"
  fi

  if [ -z "${DST_FILENAME}" ]; then
    DST_FILENAME=${SRC_FILENAME}
  fi

  FILE_URL="${KUBO_CI_URL}/${SRC_FILENAME}"

  curl -f -s -H 'Cache-Control: no-cache' "${FILE_URL}" -o ${DST_FOLDER}/${DST_FILENAME}
  chmod +x ${DST_FOLDER}/*.sh

}


#
# Cluster Files
#

echo -n "Downloading cluster files... " >> ${LOG_FILE}
download_file "cluster.service" "${CLUSTER_HOME_FOLDER}"
download_file "cluster.sh" "${CLUSTER_HOME_FOLDER}"
echo "OK"  >> ${LOG_FILE}

#
# Initialize Microk8s
#

echo -n "Initializing microk8s... " >> ${LOG_FILE}

# Enable plugins
microk8s status --wait-ready
microk8s enable dashboard 
microk8s enable dns 
microk8s enable helm
microk8s enable ingress

# Install NFS driver
microk8s helm3 repo add csi-driver-nfs https://raw.githubusercontent.com/kubernetes-csi/csi-driver-nfs/master/charts
microk8s helm3 repo update
microk8s helm3 install csi-driver-nfs csi-driver-nfs/csi-driver-nfs \
    --namespace kube-system \
    --set kubeletDir=/var/snap/microk8s/common/var/lib/kubelet
microk8s kubectl wait pod --selector app.kubernetes.io/name=csi-driver-nfs --for condition=ready --namespace kube-system

# Create kubok8s-nfs StorageClass
if [ ! -e /mnt/data/pv ]; then
  mkdir -p /mnt/data/pv
fi
CHECK_STORAGE=$( microk8s kubectl get storageclass -A | grep "kubok8s-nfs" )
if [ -z "${CHECK_STORAGE}" ]; then
  microk8s kubectl apply -f - <<EOF
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: kubok8s-nfs
provisioner: nfs.csi.k8s.io
parameters:
  server: ${CLUSTER_NFS_SERVER}
  share: /pv
reclaimPolicy: Delete
volumeBindingMode: Immediate
mountOptions:
  - hard
  - nfsvers=4.1
EOF
fi

# Create kuboadmin user
CHECK_USER=$( microk8s kubectl get serviceaccounts -A | grep "kuboadmin" )
if [ -z "${CHECK_USER}" ]; then
  microk8s kubectl -n kube-system create serviceaccount kuboadmin
  microk8s kubectl create clusterrolebinding kuboadmin --clusterrole=cluster-admin --serviceaccount=kube-system:kuboadmin
  API_TOKEN=$( microk8s kubectl -n kube-system create token kuboadmin --duration=8760h )
  if [ ! -f ${CLUSTER_CONFIG_FOLDER}/kubeconfig ]; then
  cat > ${CLUSTER_CONFIG_FOLDER}/kubeconfig << EOF
apiVersion: v1
clusters:
- name: kubok8s-cluster
  cluster:
    server: https://mgmt.${DEFAULT_DOMAIN}/
contexts:
- name: kubok8s
  context:
    cluster: kubok8s-cluster
    user: kuboadmin
current-context: kubok8s
kind: Config
preferences: {}
users:
- name: kuboadmin
  user:
    token: ${API_TOKEN}
EOF
  fi
  aws s3 cp ${CLUSTER_CONFIG_FOLDER}/kubeconfig s3://${KUBO_STATE_BUCKET}/files/kubeconfig
fi
echo "OK"  >> ${LOG_FILE}

#
# Add Systemd Cluster Service
#

echo -n "Adding cluster service... " >> ${LOG_FILE}
if [ -e ${CLUSTER_HOME_FOLDER}/cluster.service ]; then
  cp ${CLUSTER_HOME_FOLDER}/cluster.service /usr/lib/systemd/system/
  systemctl daemon-reload
  systemctl enable cluster.service
  echo "OK" >> ${LOG_FILE}
else
  echo "FAILED" >> ${LOG_FILE}
  exit
fi

#
# Update cluster environment
#
echo "CLUSTER_HOME_FOLDER='${CLUSTER_HOME_FOLDER}'" >> ${CLUSTER_HOME_FOLDER}/cluster.env
echo "CLUSTER_DATA_FOLDER='${CLUSTER_DATA_FOLDER}'" >> ${CLUSTER_HOME_FOLDER}/cluster.env
echo "CLUSTER_CONFIG_FOLDER='${CLUSTER_CONFIG_FOLDER}'" >> ${CLUSTER_HOME_FOLDER}/cluster.env
echo "CLUSTER_VOLUME_FOLDER='${CLUSTER_VOLUME_FOLDER}'" >> ${CLUSTER_HOME_FOLDER}/cluster.env
echo "CLUSTER_LOCK_FILE='${CLUSTER_LOCK_FILE}'" >> ${CLUSTER_HOME_FOLDER}/cluster.env
echo "CLUSTER_NODE_FILE='${CLUSTER_NODE_FILE}'" >> ${CLUSTER_HOME_FOLDER}/cluster.env
echo "CLUSTER_NFS_SERVER='${CLUSTER_NFS_SERVER}'" >> ${CLUSTER_HOME_FOLDER}/cluster.env
echo "CLUSTER_NFS_VOLUME='${CLUSTER_NFS_VOLUME}'" >> ${CLUSTER_HOME_FOLDER}/cluster.env

#
# Upload NFS configuration
#
echo "CLUSTER_VOLUME_FOLDER='${CLUSTER_VOLUME_FOLDER}'" >> ${CLUSTER_HOME_FOLDER}/nfsconfig.env
echo "CLUSTER_NFS_SERVER='${CLUSTER_NFS_SERVER}'" > ${CLUSTER_HOME_FOLDER}/nfsconfig.env
echo "CLUSTER_NFS_VOLUME='${CLUSTER_NFS_VOLUME}'" >> ${CLUSTER_HOME_FOLDER}/nfsconfig.env
aws s3 cp ${CLUSTER_HOME_FOLDER}/nfsconfig.env s3://${KUBO_STATE_BUCKET}/files/nfsconfig.env

#
# Start cluster service
#
echo -n "Starting cluster service... " >> ${LOG_FILE}
systemctl start cluster.service
echo "OK" >> ${LOG_FILE}

echo "Cluster node initialization completed." >> ${LOG_FILE}
