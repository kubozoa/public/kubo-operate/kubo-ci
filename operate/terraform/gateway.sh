#!/bin/bash

COMMAND="$1"

# Import Environment
GATEWAY_HOME_FOLDER="/opt/kubozoa"
if [ -f ${GATEWAY_HOME_FOLDER}/gateway.env ]; then
  log "Importing environment: ${GATEWAY_HOME_FOLDER}/gateway.env"
  source ${GATEWAY_HOME_FOLDER}/gateway.env
else
  log "Missing gateway environment file: ${GATEWAY_HOME_FOLDER}/gateway.env"
fi

DEFAULT_IP_ADDRESS="$( ip route | grep default | cut -d' ' -f9 )"

if [ -n "${DEFAULT_DOMAIN}" ]; then
  export DEFAULT_DOMAIN
else
  echo "No service domain defined, exiting."
  exit 1
fi

if [ -n "${DEFAULT_IP_ADDRESS}" ]; then
  export DEFAULT_IP_ADDRESS
else
  echo "No default IP address defined, exiting."
  exit 1
fi

if [ "${COMMAND}" = "start" ]; then
  exec docker stack deploy --compose-file ${GATEWAY_HOME_FOLDER}/gateway.yml --detach=true gateway
elif [ "${COMMAND}" = "stop" ]; then
  exec docker stack rm gateway
else
  echo "Invalid command!"
fi
