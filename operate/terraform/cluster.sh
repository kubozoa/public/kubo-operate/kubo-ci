#!/bin/bash

# Script Settings
SCRIPT=$( realpath "$0" )
SCRIPT_NAME=$( basename "${SCRIPT}" )
LOG_FILE="/var/log/${SCRIPT_NAME%.*}.log"

# Log functions
function log() {
  local MESSAGE=$1
  echo "$( date +"%Y%m%d-%H%M%S" ): ${MESSAGE}" >> ${LOG_FILE}
}

# Import Environment
CLUSTER_HOME_FOLDER="/opt/kubozoa"
if [ -f ${CLUSTER_HOME_FOLDER}/cluster.env ]; then
  log "Importing environment: ${CLUSTER_HOME_FOLDER}/cluster.env"
  source ${CLUSTER_HOME_FOLDER}/cluster.env
else
  log "Missing cluster environment file: ${CLUSTER_HOME_FOLDER}/cluster.env"
fi

# Cluster functions
function log_event() {
  local MESSAGE=$1
  echo "$( date +"%Y%m%d-%H%M%S" ): ${MESSAGE}" >> ${CLUSTER_CONFIG_FOLDER}/events
}

function get_local_ip() {
  log "Getting local IP..."
  LOCAL_IP=$( hostname -I | awk '{print $1}' )
}

acquire_lock() {
  # Optional arguments
  local timeout="${1:-30}"  # Default timeout of 30 seconds
  local max_age="${2:-60}"  # Default max lock age of 60 seconds

  # Local variables
  local start_time=$(date +%s)
  local current_time
  local pid=$$
  local hostname=$(hostname)
  local lock_content="${pid}@${hostname}"

  # Try to acquire lock until timeout
  while true; do
      # Attempt to create lock file atomically
      if (set -o noclobber; echo "${lock_content}" > "${CLUSTER_LOCK_FILE}") 2>/dev/null; then
          # Successfully acquired lock
          trap 'rm -f "$lock_file"; exit' INT TERM EXIT
          log "Lock acquired by ${lock_content}" >&2
          return 0
      fi
      # Check if we exceeded timeout
      current_time=$(date +%s)
      if [ $((current_time - start_time)) -ge "$timeout" ]; then
          log "Error: Timeout waiting for lock after ${timeout} seconds" >&2
          exit 1
      fi
      # Check for stale lock based on file modification time
      if [ -f "${CLUSTER_LOCK_FILE}" ]; then
          local file_age=$((current_time - $(stat -c %Y "${CLUSTER_LOCK_FILE}" 2>/dev/null || echo 0)))
          if [ "$file_age" -ge "$max_age" ]; then
              log "Removing stale lock file (age: ${file_age}s, max: ${max_age}s)" >&2
              # Attempt to remove stale lock and retry
              rm -f "${CLUSTER_LOCK_FILE}"
          fi
      fi
      # Wait briefly before retrying
      sleep 1
  done
}

function release_lock () {
  log "Releasing lock..."
  rm -f ${CLUSTER_LOCK_FILE}
}

function get_manager_info () {
  local CURRENT_MANAGER=0
  MANAGER_ADDRESS=""
  NUM_MANAGERS="0"
  if [ -e ${CLUSTER_CONFIG_FOLDER}/manager ]; then
    NUM_MANAGERS=$( cat ${CLUSTER_CONFIG_FOLDER}/manager | wc -l )
    if [[ ${NUM_MANAGERS} -gt 0 ]]; then
      while [ "${MANAGER_ADDRESS}" = "" ] && [[ ${CURRENT_MANAGER} -lt ${NUM_MANAGERS} ]]; do
        CURRENT_MANAGER=$(( CURRENT_MANAGER + 1 ))
        CURRENT_ADDRESS=$( head -${CURRENT_MANAGER} ${CLUSTER_CONFIG_FOLDER}/manager | tail -1 )
        log "Checking manager node ${CURRENT_MANAGER}: ${CURRENT_ADDRESS}"
        ping -c 1 ${CURRENT_ADDRESS} > /dev/null 2>&1
        STATUS=$?
        if [ "${STATUS}" = "0" ]; then
          log "Using manager node: ${CURRENT_ADDRESS}"
          MANAGER_ADDRESS="${CURRENT_ADDRESS}"
          return 0
        fi
      done
    fi
  fi
}

function update_nodes () {
  local ROLE=$1
  local ADDRESS=$2
  NODE_ENTRY=""
  if [ -e ${CLUSTER_CONFIG_FOLDER}/${ROLE} ]; then
    NODE_ENTRY=$( cat ${CLUSTER_CONFIG_FOLDER}/${ROLE} | grep ${ADDRESS} )
  fi
  if [ "${NODE_ENTRY}" = "" ]; then
    echo "${ADDRESS}" >> ${CLUSTER_CONFIG_FOLDER}/${ROLE}
  fi
  echo "${ROLE}" > ${CLUSTER_NODE_FILE}
}

function create_token() {
  if [ ! -e ${CLUSTER_CONFIG_FOLDER}/token ]; then
    log "Creating new join token..."
    echo $( microk8s add-node | grep 25000 | head -1 | cut -d':' -f2 ) > ${CLUSTER_CONFIG_FOLDER}/token
    chmod 600 ${CLUSTER_CONFIG_FOLDER}/token
  fi
}

function get_token () {
  if [ -e ${CLUSTER_CONFIG_FOLDER}/token ]; then
    TOKEN=$( cat ${CLUSTER_CONFIG_FOLDER}/token )
    rm -f ${CLUSTER_CONFIG_FOLDER}/token
  fi
}

function init_cluster() {

  log "Initializing cluster..."

  create_token
  update_nodes "manager" "${LOCAL_IP}"

  log_event "${LOCAL_IP} - Starting: Initialized the cluster"
}

function join_cluster() {

  log "Joining cluster..."

  if [ "${MANAGER_ADDRESS}" != "" ]; then
    if [[ ${NUM_MANAGERS} -lt ${CLUSTER_MANAGERS} ]]; then
      NODE_ROLE="manager"
      WORKER_FLAG=""
    else
      NODE_ROLE="worker"
      WORKER_FLAG="--worker"
    fi
    log "Node role: ${NODE_ROLE}"
    get_token
    if [ -n "${TOKEN}" ]; then
      log "Joining cluster with token: ${TOKEN}"
      microk8s join --skip-verify ${MANAGER_ADDRESS}:${TOKEN} ${WORKER_FLAG} >> ${LOG_FILE} 2>&1
      update_nodes "${NODE_ROLE}" "${LOCAL_IP}"
      log_event "${LOCAL_IP} - Starting: Joined the cluster as ${NODE_ROLE}"
    else
      log "Joining cluster failed, no token available!"
    fi
  else
    log_event "${LOCAL_IP} - Starting: Join cluster failed, no manager available!"
  fi
}

function leave_cluster() {

  log "Leaving cluster..."

  microk8s leave >> ${LOG_FILE} 2>&1
  log "Removing node entries for ${LOCAL_IP}"
  if [ -e ${CLUSTER_CONFIG_FOLDER}/manager ]; then
    sed -i "/${LOCAL_IP}/d" ${CLUSTER_CONFIG_FOLDER}/manager
  fi
  if [ -e ${CLUSTER_CONFIG_FOLDER}/worker ]; then
    sed -i "/${LOCAL_IP}/d" ${CLUSTER_CONFIG_FOLDER}/worker
  fi
  rm -f ${CLUSTER_NODE_FILE}

  log_event "${LOCAL_IP} - Stopping: Node left the cluster"
}

function monitor_node() {

  log "Checking node..."
 
  if [ -e ${CLUSTER_NODE_FILE} ]; then
    ROLE=$( cat ${CLUSTER_NODE_FILE} )
    log "Cluster node: OK (${ROLE})"
  else
    log "Cluster node: INIT"
    systemctl restart cluster.service
  fi
}

function monitor_cluster() {

  if [ "${LOCAL_IP}" = "${MANAGER_ADDRESS}" ]; then
    log "Checking cluster..."
    create_token
    rm -f /tmp/manager /tmp/worker
    NODE_LIST=( $( microk8s kubectl get nodes | grep 'NotReady' | cut -d' ' -f1 ) )
    for NODE in "${NODE_LIST[@]}"; do
      if [ -n "$( microk8s kubectl describe nodes ${NODE} | grep 'microk8s-worker' )" ]; then
        ROLE="worker"
      else
        ROLE="manager"
      fi
      ADDRESS=$( microk8s kubectl describe nodes ${NODE} | grep 'InternalIP' | cut -d' ' -f5 )
      STATE="NotReady"
      log "Node: ${ADDRESS} - Role: ${ROLE} - State: ${STATE}"
      microk8s remove-node ${NODE} >> ${LOG_FILE} 2>&1
      log_event "${ADDRESS} - Cleanup: Removed from cluster"
    done
    NODE_LIST=( $( microk8s kubectl get nodes | grep Ready | cut -d' ' -f1 ) )
    for NODE in "${NODE_LIST[@]}"; do
      if [ -n "$( microk8s kubectl describe nodes ${NODE} | grep 'microk8s-worker' )" ]; then
        ROLE="worker"
      else
        ROLE="manager"
      fi
      ADDRESS=$( microk8s kubectl describe nodes ${NODE} | grep 'InternalIP' | cut -d' ' -f5 )
      STATE="Ready"
      log "Node: ${ADDRESS} - Role: ${ROLE} - State: ${STATE}"
      echo "${ADDRESS}" >> /tmp/${ROLE}
    done
    if [ -e /tmp/manager ]; then
      mv /tmp/manager ${CLUSTER_CONFIG_FOLDER}
    else
      rm -f ${CLUSTER_CONFIG_FOLDER}/manager
    fi
    if [ -e /tmp/worker ]; then
      mv /tmp/worker ${CLUSTER_CONFIG_FOLDER}
    else
      rm -f ${CLUSTER_CONFIG_FOLDER}/worker
    fi
  fi
}

# Cluster node actions
COMMAND="$1"

# Get local node IP address
get_local_ip

# Start/stop node
if [ "${COMMAND}" = "monitor" ]; then

  monitor_node

  acquire_lock

  get_manager_info
  monitor_cluster

  release_lock

elif [ "${COMMAND}" = "start" ]; then

  log_event "${LOCAL_IP} - Cluster node: ${COMMAND}"

  acquire_lock

  get_manager_info
  if [ "${MANAGER_ADDRESS}" = "" ]; then
    init_cluster
  else
    join_cluster
  fi

  release_lock

  log "Scheduling cluster node monitor... "
  if [ -e ${CLUSTER_HOME_FOLDER}/cluster.sh ] && [ "$( head -1 ${CLUSTER_HOME_FOLDER}/cluster.sh )" = "#!/bin/bash" ]; then
    ROLE=$( cat ${CLUSTER_NODE_FILE} )
    if [ "${ROLE}" = "manager" ]; then
      SCHEDULE="*/2 * * * *"
    else
      SCHEDULE="*/5 * * * *"
    fi
    echo "${SCHEDULE} ${CLUSTER_HOME_FOLDER}/cluster.sh monitor" | crontab -
  fi

  log "Cluster node start completed."

elif [ "${COMMAND}" = "stop" ]; then

  log_event "${LOCAL_IP} - Cluster node: ${COMMAND}"

  log "Un-scheduling cluster node monitor... "
  if [ -e ${CLUSTER_HOME_FOLDER}/cluster.sh ] && [ "$( head -1 ${CLUSTER_HOME_FOLDER}/cluster.sh )" = "#!/bin/bash" ]; then
    echo "# * * * * * ${CLUSTER_HOME_FOLDER}/cluster.sh monitor" | crontab -
  fi

  acquire_lock

  leave_cluster

  release_lock

  log "Cluster node stop completed."

fi
