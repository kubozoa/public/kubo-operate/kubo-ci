#!/bin/bash

# Set node type
NODE_TYPE="{{node-type}}"
if [ "${NODE_TYPE}" = "gateway" ] || [ "${NODE_TYPE}" = "cluster" ]; then
  echo "Node type: '${NODE_TYPE}'"
else
  echo "Invalid node type: '${NODE_TYPE}'"
  exit 1
fi

# Download node-specific script
echo -n "Downloading node-init script for '${NODE_TYPE}'... " >> /var/log/user-data.log
curl -s -X GET "https://gitlab.com/kubozoa/public/kubo-operate/kubo-ci/raw/main/operate/terraform/${NODE_TYPE}-init.sh" -o /tmp/node-init.sh
if [ -f /tmp/node-init.sh ] && [ -n "$( grep bash /tmp/node-init.sh )" ]; then
  echo "OK" >> /var/log/user-data.log
else
  echo "FAILED" >> /var/log/user-data.log
  echo -n "Error: "
  cat /tmp/node-init.sh >> /var/log/user-data.log
  exit 1
fi

# Execute node-init script
echo -n "Executing node-init... " >> /var/log/user-data.log
chmod +x /tmp/node-init.sh
/tmp/node-init.sh "GCP"
echo "OK" >> /var/log/user-data.log
