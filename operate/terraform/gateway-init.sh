#!/bin/bash

# Script Settings
SCRIPT=$( realpath "$0" )
SCRIPT_NAME=$( basename "${SCRIPT}" )
LOG_FILE="/var/log/${SCRIPT_NAME%.*}.log"

# Gateway settings
GATEWAY_HOME_FOLDER="/opt/kubozoa"
GATEWAY_DATA_FOLDER="/mnt/data"
if [ -d ${GATEWAY_HOME_FOLDER} ]; then
  echo "Gateway home folder found: ${GATEWAY_HOME_FOLDER}" >> ${LOG_FILE}
else
  mkdir -p ${GATEWAY_HOME_FOLDER}
fi
if [ -d ${GATEWAY_DATA_FOLDER} ]; then
  echo "Gateway data folder found: ${GATEWAY_DATA_FOLDER}" >> ${LOG_FILE}
else
  mkdir -p ${GATEWAY_DATA_FOLDER}
fi

# Gateway environment
source ${GATEWAY_HOME_FOLDER}/gateway.env

# Gateway services
mkdir -p ${GATEWAY_HOME_FOLDER}/traefik
chmod 777 ${GATEWAY_HOME_FOLDER}/traefik

# Kubo-CI URL
KUBO_CI_URL="https://gitlab.com/kubozoa/public/kubo-operate/kubo-ci/raw/main/operate/terraform"

# Versions
KUBECTL_VERSION="v1.31.0"

#
# Kernel Parameters
#

echo -n "Updating kernel parameters... " >> ${LOG_FILE}
echo "vm.max_map_count=262144" > /etc/sysctl.d/gateway.conf
echo "vm.swappiness=20" >> /etc/sysctl.d/gateway.conf
sysctl --system
echo "OK" >> ${LOG_FILE}

#
# Disable Firewall Service
#

echo -n "Disabling firewall service... " >> ${LOG_FILE}
if [ -n "$( which ufw )" ]; then
  systemctl stop ufw
  systemctl disable ufw
elif [ -n "$( which firewalld )" ]; then
  systemctl stop firewalld
  systemctl disable firewalld
fi
echo "OK" >> ${LOG_FILE}

#
# Swap Space
#

echo -n "Creating and mounting swap... " >> ${LOG_FILE}
fallocate -l 2g /mnt/2GiB.swap
chmod 600 /mnt/2GiB.swap
mkswap /mnt/2GiB.swap
swapon /mnt/2GiB.swap
echo '/mnt/2GiB.swap swap swap defaults 0 0' | sudo tee -a /etc/fstab
echo "OK" >> ${LOG_FILE}

#
# Install packages
#

echo "Installing packages:" >> ${LOG_FILE}
curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
while [ -n "$( ps -ef | grep ' apt' | grep -v 'grep' )" ]; do
  echo "Waiting for lock..."
  sleep 5
done
apt-get -o DPkg::Lock::Timeout=300 -y update >> ${LOG_FILE} 2>&1
# Docker and NFS
echo "-> Docker and NFS..."
apt-get -o DPkg::Lock::Timeout=300 -y install containerd.io docker-ce docker-ce-cli net-tools nfs-common unzip >> ${LOG_FILE} 2>&1
sed -i "s|ExecStart=.*|ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:7776 --containerd=/run/containerd/containerd.sock|g" /usr/lib/systemd/system/docker.service  >> ${LOG_FILE} 2>&1
systemctl daemon-reload >> ${LOG_FILE} 2>&1
systemctl restart docker.service >> ${LOG_FILE} 2>&1
# AWS CLI
echo "-> AWS CLI..."
curl -Ls https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o /tmp/awscliv2.zip >> ${LOG_FILE} 2>&1
unzip -q /tmp/awscliv2.zip -d /tmp >> ${LOG_FILE} 2>&1
/tmp/aws/install >> ${LOG_FILE} 2>&1
# kubectl
echo "-> kubectl..."
curl -Ls "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" -o /tmp/kubectl >> ${LOG_FILE} 2>&1
install -o root -g root -m 0755 /tmp/kubectl /usr/local/bin/kubectl >> ${LOG_FILE} 2>&1

#
# Mount EFS Volume (if defined)
#

if [ -n "${EFS_VOLUME_ID}" ]; then
  AZ=$( curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone )
  REGION="${AZ%?}"
  GATEWAY_NFS_SERVER="${AZ}.${EFS_VOLUME_ID}.efs.${REGION}.amazonaws.com"
  echo -n "Mounting cluster folder (ID: ${EFS_VOLUME_ID})... " >> ${LOG_FILE}
  echo "${GATEWAY_NFS_SERVER}:/ ${GATEWAY_DATA_FOLDER} nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0" | tee -a /etc/fstab
  mount -a
  if [ "$( mount | grep ${GATEWAY_DATA_FOLDER} )" = "" ]; then
    echo "FAILED" >> ${LOG_FILE}
    exit
  fi
  echo "OK" >> ${LOG_FILE}
fi

#
# Download Files
#

function download_file() {

  local SRC_FILENAME="$1"
  local DST_FOLDER="$2"
  local DST_FILENAME="$3"

  if [ ! -d "${DST_FOLDER}" ]; then
    mkdir -p "${DST_FOLDER}"
    chmod 777 "${DST_FOLDER}"
  fi

  if [ -z "${DST_FILENAME}" ]; then
    DST_FILENAME=${SRC_FILENAME}
  fi

  FILE_URL="${KUBO_CI_URL}/${SRC_FILENAME}"

  curl -f -s -H 'Cache-Control: no-cache' "${FILE_URL}" -o ${DST_FOLDER}/${DST_FILENAME}
  chmod +x ${DST_FOLDER}/*.sh

}

#
# Gateway Files
#

echo -n "Downloading gateway files... " >> ${LOG_FILE}
download_file "gateway.service" "${GATEWAY_HOME_FOLDER}"
download_file "gateway.sh" "${GATEWAY_HOME_FOLDER}"
download_file "gateway.yml" "${GATEWAY_HOME_FOLDER}"
echo "OK"  >> ${LOG_FILE}

#
# Initialize swarm
#

echo "Initializing docker swarm..." >> ${LOG_FILE}
docker swarm init >> ${LOG_FILE} 2>&1

#
# Add Systemd Gateway Service
#

echo -n "Adding gateway service... " >> ${LOG_FILE}
if [ -e ${GATEWAY_HOME_FOLDER}/gateway.service ]; then
  cp ${GATEWAY_HOME_FOLDER}/gateway.service /usr/lib/systemd/system/
  systemctl daemon-reload
  systemctl enable gateway.service
  echo "OK" >> ${LOG_FILE}
else
  echo "FAILED" >> ${LOG_FILE}
  exit
fi

#
# Update gateway environment
#
echo "GATEWAY_HOME_FOLDER='${GATEWAY_HOME_FOLDER}'" >> ${GATEWAY_HOME_FOLDER}/gateway.env
echo "GATEWAY_DATA_FOLDER='${GATEWAY_DATA_FOLDER}'" >> ${GATEWAY_HOME_FOLDER}/gateway.env
echo "GATEWAY_NFS_SERVER='${GATEWAY_NFS_SERVER}'" >> ${GATEWAY_HOME_FOLDER}/gateway.env

#
# Start gateway service
#
echo -n "Starting gateway service... " >> ${LOG_FILE}
systemctl start gateway.service
echo "OK" >> ${LOG_FILE}

echo "Gateway node initialization completed." >> ${LOG_FILE}
