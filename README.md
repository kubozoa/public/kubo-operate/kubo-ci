<a href="https://www.kubozoa.com"><img src="https://assets.kubozoa.com/logo/kubozoa-logo-color-black.png" height="60" alt="Kubozoa Inc." /></a>

<p>&nbsp;</p>

# Kubo-CI 

Kubo-CI is a Gitlab-CI based framework that automatically triggers pipelines for DevSecOps based on project content and configurations provided by the user.

Its aim is to speed up project setup and ensure consistency across different projects within an organization.

# Features

The three main components of KuboCI provide the following:

* DEVELOP: Automated pipelines for development projects supporting the following providers:
  - Go
  - Image
  - Java
  - KuboCI Module Packaging (shell, terraform)
  - NodeJS
  - Python
* SECURE: Automated pipeline for security tools
* OPERATE: Automated pipeline for operations projects to deploy/remove modules supporting the following providers:
  - Shell
  - Terraform

## Requirements

KuboCI is designed to be as light as possible on requirements:
- Gitlab v17 or later
- A Gitlab Runner with Docker or Kubernetes executor

# Table of Contents

- [Getting Started](#getting-started)
- [Provider Pipelines](#provider-pipelines)
  - [Develop](#develop-provider-pipelines)
  - [Secure](#secure-provider-pipelines)
  - [Operate](#operate-provider-pipelines)

# Getting Started

1. Copy variables.yml and gitlab-ci.yml to your project as .variables.yml and .gitlab-ci.yml
2. Enable the components required for the project in .variables.yml
3. As needed, set the module variables for each module in .variables.yml (NOTE: the complete list of configuration options can be found in the template variables.yml)

## Kubo-CI Main Configuration

| Environment Variable | Description | Value(s) | Default |
| -------------------- | ----------- | -------- | ------- |
| KUBO_CI_DEVELOP | Enable Develop component. All environment variables exposed by this component are prefixed with `DEV_` | "true" or "false" | "false"
| KUBO_CI_SECURE | Enable Secure component. All environment variables exposed by this component are prefixed with `SEC_` | "true" or "false" | "false"
| KUBO_CI_OPERATE | Enable Operate component. All environment variables exposed by this component are prefixed with `OPS_` | "true" or "false" | "false"
| KUBO_CI_DEBUG | Enable debug logging. | "true" or "false" | "false"
| KUBO_CI_RUNNER_TAG | Set a runner tag for the KuboCI pipelines | User Defined | "kubo-ci"

NOTE: For the runner tag, users can either add the "kubo-ci" tag to their existing runners or set the KUBO_CI_RUNNER_TAG to a tag supported by their runners.

# Provider Pipelines

The following is a short description of the available providers for each main component. The full pipeline description and its configuration options can be found in the template variable.yml.

## Develop Provider Pipelines

### Go

### Image

The image provider will build a docker image from the image build files of the project. The built image can then be pushed to an image registry.

### Java

### Module Packaging

The module packaging provider will validate and package a module and its template and upload them to a package registry.

### NodeJS

### Python

## Secure Provider Pipelines

## Operate Provider Pipelines

The Operate component works differently than the other two components. Instead of fixed single-provider pipelines, the Operate component will generate dynamic pipelines to deploy or remove a series of modules (shell or terraform). 

The general structure of an Operate project follows a simple directory structure:
* <deployment_root>/<name_of_deployment>/<module_name>/

When the pipeline executes, it will generate two dynamic pipelines to deploy and remove the modules listed in the configuration. In addition to features available via the variables.yml settings, the dynamic pipelines provide the following functionality:
- Artifacts transmission: each job can store artifacts that are available to downstream jobs

Modules written for KuboCI can leverage built-in features of the framework:
- Deployment and module variables
- Vault for sensitive information
- Utility library

For more information on the features available to modules in KuboCI, please refer to the templates for each module type.

# Troubleshooting

* If you encounter any errors related to missing packages or incorrect configuration, double-check your ensure that you have the necessary permissions to access the Docker registry and Gitlab CI API's.

# License

See `LICENSE` for more information.

# Acknowledgements

* https://docs.gitlab.com/ee/user/application_security/sast/

# Contact Kubozoa

Visit us at: <a href="https://www.kubozoa.com" target="_blank" title="Kubozoa Inc.">kubozoa.com</a>

Contact us at: [support@kubozoa.com](mailto:support@kubozoa.com)

<div align="center">
<hr/>
<a href="https://www.kubozoa.com"><img src="https://assets.kubozoa.com/logo/kubozoa-logo-nr-color.png" width="40" alt="Kubozoa"></a>
</div>